import json

def parse_input(data=None):
    if data is None:
        return False
    try:
        data = json.loads(data)
        return data
    except Exception as e:
        print("ERROR_PARSE_INPUT: %s" % (e))
        return False
