# pages_test
Settings->Project Name->51aba.gitlab.io

- создаем coverage html 
- кладем его в htmlcov/subdir/
- в .gitlab-ci.yml копируем htmlcov/ в public/
- там же в enviroments -> url прописываем url для доступа к странице из вебинтерфейса gitlab

- пример:
```
pages: 
    script: 
        - cp -r htmlcov/ public/
    artifacts:
        paths:
        - public
    environment:
        name: test
        url: https://51aba.gitlab.io/project_name/<BRANCH>/
```
